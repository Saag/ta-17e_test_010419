module.exports = function capitalize(input) {
	if (typeof input !== "string") {
		throw new Error("bad input");
	}
	input = input.toLowerCase();
	return input.charAt(0).toUpperCase() + input.substring(1);
};
